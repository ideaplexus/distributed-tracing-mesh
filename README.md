
## Install python dependencies

```shell
pip install -r requirements.txt
```

## Run Ansible playbook

```shell
ansible-playbook k3d-dev.yml

# or specific tag(s)

ansible-playbook k3d-dev.yml --tags "linkerd"

```
