package example

import "embed"

//go:generate npm --prefix ui ci
//go:generate npm --prefix ui run build

// UI embedded ui
//
//go:embed ui/dist
var UI embed.FS
