package client

import (
	"fmt"
	"os"
	"strings"

	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcZap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpcRetry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"example/proto/gen/go/message/v1"
)

type ClientsList map[string]message.MessageServiceClient

type GrpcClientsService struct {
	clients ClientsList
}

func NewGrpcClientsService(serviceName string, logger *zap.Logger) *GrpcClientsService {
	return &GrpcClientsService{
		clients: initClients(serviceName, logger),
	}
}

func initClients(serviceName string, logger *zap.Logger) ClientsList {
	clients := make(ClientsList)

	clientNamesString := os.Getenv(fmt.Sprintf("%s_GRPC_CLIENTS", strings.ToUpper(serviceName)))
	if len(clientNamesString) > 0 {
		clientNames := strings.Split(clientNamesString, ",")
		for _, clientName := range clientNames {
			clients[clientName] = message.NewMessageServiceClient(createClientConnection(clientName, logger))
		}
	}

	return clients
}

func createClientConnection(clientName string, logger *zap.Logger) *grpc.ClientConn {
	clientAddr := os.Getenv(fmt.Sprintf("%s_GRPC_ADDR", strings.ToUpper(clientName)))

	logger.Info(fmt.Sprintf("create connection to %s on %s", clientName, clientAddr))

	clientLogger := logger.Named(clientName)

	conn, err := grpc.Dial(clientAddr, grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(grpcMiddleware.ChainUnaryClient(
			otelgrpc.UnaryClientInterceptor(),
			grpcPrometheus.UnaryClientInterceptor,
			grpcZap.UnaryClientInterceptor(clientLogger),
			grpcRetry.UnaryClientInterceptor(),
		)),
		grpc.WithStreamInterceptor(grpcMiddleware.ChainStreamClient(
			otelgrpc.StreamClientInterceptor(),
			grpcPrometheus.StreamClientInterceptor,
			grpcZap.StreamClientInterceptor(clientLogger),
			grpcRetry.StreamClientInterceptor(),
		)),
	)
	if err != nil {
		panic(fmt.Sprintf("could not create connection to %s: %s", clientName, err))
	}

	return conn
}

func (g *GrpcClientsService) GetClients() ClientsList {
	return g.clients
}
