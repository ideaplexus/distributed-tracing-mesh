package server

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/felixge/fgprof"
	"go.uber.org/zap"
)

type PProfServer struct {
	logger *zap.Logger
	server *http.Server
}

func NewPProfServer(logger *zap.Logger) *PProfServer {
	return &PProfServer{
		logger: logger,
	}
}

func (p *PProfServer) ServePProf() error {
	mux := http.NewServeMux()

	mux.Handle("/debug/fgprof", fgprof.Handler())

	p.logger.Info(fmt.Sprintf("serving pprof on %s/debug/fgprof", os.Getenv("PPROF_ADDR")))

	server := &http.Server{
		Addr:              os.Getenv("PPROF_ADDR"),
		Handler:           mux,
		ReadHeaderTimeout: Timeout,
	}

	p.server = server

	return server.ListenAndServe()
}

func (p *PProfServer) Shutdown(ctx context.Context) {
	if err := p.server.Shutdown(ctx); err != nil {
		p.logger.Fatal("failed to shutdown pprof", zap.Error(err))
	}
}
