package server

import (
	"context"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"strings"

	loggingmiddelware "github.com/faabiosr/echo-middleware"
	"github.com/labstack/echo/v4"
	"go.uber.org/zap"

	"example"
)

type hookedResponseWriter struct {
	http.ResponseWriter
	got404 bool
}

type UIServer struct {
	serviceName string
	logger      *zap.Logger
	server      *echo.Echo
}

func NewUIServer(serviceName string, logger *zap.Logger) *UIServer {
	return &UIServer{
		serviceName: serviceName,
		logger:      logger,
	}
}

// ServeUI serve the ui.
func (u *UIServer) ServeUI() error {
	server := echo.New()
	server.HideBanner = true
	server.HidePort = true

	server.Use(loggingmiddelware.ZapLogWithConfig(loggingmiddelware.ZapLogConfig{
		Logger: u.logger.Named("request"),
	}))

	stripped, _ := fs.Sub(example.UI, "ui/dist")
	httpFS := http.FS(stripped)
	fileServer := http.FileServer(httpFS)
	serveIndex := serveFileContents("index.html", httpFS)

	server.GET("/*", echo.WrapHandler(intercept404(fileServer, serveIndex)))

	u.logger.Info(fmt.Sprintf("serving ui on %s", os.Getenv("UI_ADDR")))

	u.server = server

	return server.Start(os.Getenv("UI_ADDR"))
}

func (u *UIServer) Shutdown(ctx context.Context) {
	if err := u.server.Shutdown(ctx); err != nil {
		u.logger.Fatal("failed to shutdown ui", zap.Error(err))
	}
}

func (hrw *hookedResponseWriter) WriteHeader(status int) {
	if status == http.StatusNotFound {
		hrw.got404 = true
	} else {
		hrw.ResponseWriter.WriteHeader(status)
	}
}

func (hrw *hookedResponseWriter) Write(bytes []byte) (int, error) {
	if hrw.got404 {
		return len(bytes), nil
	}

	return hrw.ResponseWriter.Write(bytes)
}

func intercept404(handler, on404 http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		hookedWriter := &hookedResponseWriter{ResponseWriter: writer}
		handler.ServeHTTP(hookedWriter, request)

		if hookedWriter.got404 {
			on404.ServeHTTP(writer, request)
		}
	})
}

func serveFileContents(file string, files http.FileSystem) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		if !strings.Contains(request.Header.Get("Accept"), "text/html") {
			writer.WriteHeader(http.StatusNotFound)

			return
		}

		index, err := files.Open(file)
		if err != nil {
			writer.WriteHeader(http.StatusNotFound)

			return
		}

		fileInfo, err := index.Stat()
		if err != nil {
			writer.WriteHeader(http.StatusNotFound)

			return
		}

		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		http.ServeContent(writer, request, fileInfo.Name(), fileInfo.ModTime(), index)
	}
}
