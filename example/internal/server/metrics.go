package server

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

type MetricsServer struct {
	logger *zap.Logger
	server *http.Server
}

func NewMetricsServer(logger *zap.Logger) *MetricsServer {
	return &MetricsServer{
		logger: logger,
	}
}

func (m *MetricsServer) ServeMetrics() error {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())

	m.logger.Info(fmt.Sprintf("serving metrics on %s/metrics", os.Getenv("METRICS_ADDR")))

	server := &http.Server{
		Addr:              os.Getenv("METRICS_ADDR"),
		Handler:           mux,
		ReadHeaderTimeout: Timeout,
	}

	m.server = server

	return server.ListenAndServe()
}

func (m *MetricsServer) Shutdown(ctx context.Context) {
	if err := m.server.Shutdown(ctx); err != nil {
		m.logger.Fatal("failed to shutdown metrics", zap.Error(err))
	}
}
