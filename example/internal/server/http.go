package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"

	loggingmiddelware "github.com/faabiosr/echo-middleware"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	"go.opentelemetry.io/contrib/instrumentation/github.com/labstack/echo/otelecho"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
	"google.golang.org/protobuf/encoding/protojson"

	"example/internal"
	"example/internal/client"
	"example/proto/gen/go/message/v1"
)

type HTTPServer struct {
	serviceName string
	gcs         *client.GrpcClientsService
	logger      *zap.Logger
	server      *echo.Echo
}

func NewHTTPServer(serviceName string, gcs *client.GrpcClientsService, logger *zap.Logger) *HTTPServer {
	return &HTTPServer{
		serviceName: serviceName,
		logger:      logger,
		gcs:         gcs,
	}
}

func (h *HTTPServer) ServeHTTP() error {
	server := echo.New()
	server.HideBanner = true
	server.HidePort = true

	server.Use(echomiddleware.Recover())
	server.Use(echomiddleware.CORS())
	server.Use(loggingmiddelware.ZapLogWithConfig(loggingmiddelware.ZapLogConfig{
		Logger: h.logger.Named("request"),
	}))
	server.Use(otelecho.Middleware(h.serviceName))

	server.GET("/health", func(ctx echo.Context) error {
		return ctx.NoContent(http.StatusNoContent)
	})

	server.POST("/message", h.messageHandler)

	httpAddr := os.Getenv(fmt.Sprintf("%s_HTTP_ADDR", strings.ToUpper(h.serviceName)))

	h.logger.Info(fmt.Sprintf("serving http on %s", httpAddr))

	h.server = server

	return server.Start(httpAddr)
}

func (h *HTTPServer) Shutdown(ctx context.Context) {
	if err := h.server.Shutdown(ctx); err != nil {
		h.logger.Fatal("failed to shutdown http", zap.Error(err))
	}
}

func (h *HTTPServer) messageHandler(ctx echo.Context) error {
	marshalOpts := protojson.MarshalOptions{EmitUnpopulated: true}

	tracer := otel.GetTracerProvider().Tracer("echo-handler")
	tracerCtx, span := tracer.Start(ctx.Request().Context(), "http-message")

	defer span.End()

	var msg message.MessageRequest
	if err := ctx.Bind(&msg); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	if msg.Receiver == h.serviceName {
		h.logger.Info(fmt.Sprintf("received message: '%s'", msg.Message))

		return ctx.JSON(http.StatusOK, &message.MessageResponse{
			Received: true,
		})
	}

	grpcClients := h.gcs.GetClients()
	if grpcClient, ok := grpcClients[msg.Receiver]; ok {
		h.logger.Info("receiver found, forward message")

		response, err := grpcClient.Message(tracerCtx, &msg)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
		}

		return ctx.JSON(http.StatusOK, response)
	}

	if 0 < len(grpcClients) {
		h.logger.Info("receiver not found, broadcast message")

		received := internal.Broadcast(tracerCtx, &msg, grpcClients, h.logger)
		if received {
			h.logger.Info("broadcast successful, message forwarded")
		} else {
			h.logger.Info("broadcast failed, receiver not found")
		}

		json, _ := marshalOpts.Marshal(&message.MessageResponse{
			Received: received,
		})

		return ctx.JSONBlob(http.StatusOK, json)
	}

	h.logger.Info("receiver not found")

	json, _ := marshalOpts.Marshal(&message.MessageResponse{
		Received: false,
	})

	return ctx.JSONBlob(http.StatusOK, json)
}
