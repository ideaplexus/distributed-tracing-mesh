package server

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"

	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcZap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpcRecovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	"example/internal"
	"example/internal/client"
	"example/proto/gen/go/message/v1"
)

type GRPCServer struct {
	serviceName string
	gcs         *client.GrpcClientsService
	logger      *zap.Logger
	server      *grpc.Server
}

type MessageServer struct {
	message.UnimplementedMessageServiceServer
	serviceName string
	gcs         *client.GrpcClientsService
	logger      *zap.Logger
}

func NewGRPCServer(serviceName string, gcs *client.GrpcClientsService, logger *zap.Logger) *GRPCServer {
	return &GRPCServer{
		serviceName: serviceName,
		logger:      logger,
		gcs:         gcs,
	}
}

func (g *GRPCServer) ServeGRPC() error {
	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpcMiddleware.ChainUnaryServer(
			otelgrpc.UnaryServerInterceptor(),
			grpcPrometheus.UnaryServerInterceptor,
			grpcZap.UnaryServerInterceptor(g.logger),
			grpcRecovery.UnaryServerInterceptor(),
		)),
		grpc.StreamInterceptor(grpcMiddleware.ChainStreamServer(
			otelgrpc.StreamServerInterceptor(),
			grpcPrometheus.StreamServerInterceptor,
			grpcZap.StreamServerInterceptor(g.logger),
			grpcRecovery.StreamServerInterceptor(),
		)),
	)

	messageServer := NewMessageServer(g.serviceName, g.logger, g.gcs)
	message.RegisterMessageServiceServer(grpcServer, messageServer)

	serverAddr := os.Getenv(fmt.Sprintf("%s_GRPC_ADDR", strings.ToUpper(g.serviceName)))

	g.logger.Info(fmt.Sprintf("listen on %s", serverAddr))

	listen, err := net.Listen("tcp", serverAddr)
	if err != nil {
		g.logger.Fatal("failed to listen", zap.Error(err))
	}

	g.logger.Info(fmt.Sprintf("serving grpc on %s", serverAddr))

	g.server = grpcServer

	return grpcServer.Serve(listen)
}

func (g *GRPCServer) Shutdown() {
	g.server.GracefulStop()
}

func NewMessageServer(serviceName string, logger *zap.Logger, gcs *client.GrpcClientsService) *MessageServer {
	return &MessageServer{
		serviceName: serviceName,
		gcs:         gcs,
		logger:      logger,
	}
}

func (m *MessageServer) Message(ctx context.Context, msg *message.MessageRequest) (*message.MessageResponse, error) {
	tracer := otel.GetTracerProvider().Tracer(m.serviceName)
	tracerCtx, span := tracer.Start(ctx, "grpc-message")

	defer span.End()

	if msg.Receiver == m.serviceName {
		m.logger.Info(fmt.Sprintf("received message: '%s'", msg.Message))

		return &message.MessageResponse{
			Received: true,
		}, nil
	}

	clients := m.gcs.GetClients()
	if clt, ok := clients[msg.Receiver]; ok {
		m.logger.Info("receiver found, forward message")

		return clt.Message(tracerCtx, msg)
	}

	if 0 < len(clients) {
		m.logger.Info("receiver not found, broadcast message")

		received := internal.Broadcast(tracerCtx, msg, clients, m.logger)
		if received {
			m.logger.Info("broadcast successful, message forwarded")
		} else {
			m.logger.Info("broadcast failed, receiver not found")
		}

		return &message.MessageResponse{
			Received: received,
		}, nil
	}

	m.logger.Info("dropped message")

	return &message.MessageResponse{
		Received: false,
	}, nil
}
