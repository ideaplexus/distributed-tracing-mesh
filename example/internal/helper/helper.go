package helper

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func ParseBoolEnv(serviceName string, key string) bool {
	global, _ := strconv.ParseBool(os.Getenv(key))
	client, _ := strconv.ParseBool(os.Getenv(fmt.Sprintf("%s_%s", strings.ToUpper(serviceName), key)))

	return global || client
}
