package internal

import (
	"context"
	"fmt"
	"sync"

	"go.uber.org/zap"

	"example/internal/client"
	"example/proto/gen/go/message/v1"
)

func Broadcast(ctx context.Context, msg *message.MessageRequest, clients client.ClientsList, logger *zap.Logger) bool {
	received := false

	var (
		wtGrp sync.WaitGroup
		mtx   sync.Mutex
	)

	for name, clt := range clients {
		wtGrp.Add(1)

		go func(clientName string, client message.MessageServiceClient) {
			defer wtGrp.Done()

			logger.Info(fmt.Sprintf("broadcast to %s", clientName))

			response, err := client.Message(ctx, msg)
			if err == nil {
				mtx.Lock()
				defer mtx.Unlock()

				received = received || response.Received
			}
		}(name, clt)
	}

	wtGrp.Wait()

	return received
}
