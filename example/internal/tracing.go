package internal

import (
	"context"
	"os"

	"go.opentelemetry.io/contrib/propagators/b3"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.uber.org/zap"
)

func InitTracing(serviceName string, logger *zap.Logger) {
	var (
		exp trace.SpanExporter
		err error
	)

	if os.Getenv("TRACE_ENDPOINT") != "" {
		exp, err = otlptrace.New(
			context.Background(),
			otlptracegrpc.NewClient(
				otlptracegrpc.WithInsecure(),
				otlptracegrpc.WithEndpoint(os.Getenv("TRACE_ENDPOINT")),
			),
		)
	} else {
		exp, err = jaeger.New(
			jaeger.WithAgentEndpoint(
				jaeger.WithAgentHost(os.Getenv("JAEGER_AGENT_HOST")),
				jaeger.WithAgentPort(os.Getenv("JAEGER_AGENT_PORT")),
			),
		)
	}

	if err != nil {
		logger.Fatal("failed to create jaeger exporter", zap.Error(err))
	}

	resources := resource.NewWithAttributes(
		semconv.SchemaURL,
		semconv.ServiceNameKey.String(serviceName),
		semconv.HostNameKey.String(os.Getenv("HOSTNAME")),
	)

	tracerProvider := trace.NewTracerProvider(
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithResource(resources),
		trace.WithBatcher(exp),
	)

	otel.SetTracerProvider(tracerProvider)

	p := b3.New(b3.WithInjectEncoding(b3.B3MultipleHeader))
	otel.SetTextMapPropagator(p)
}
