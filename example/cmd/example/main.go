package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/subosito/gotenv"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"

	"example/internal"
	"example/internal/client"
	"example/internal/helper"
	"example/internal/server"
)

func main() {
	var serviceName string

	flag.StringVar(&serviceName, "name", "example", "The service name of this instance")
	flag.Parse()

	_ = gotenv.Load()

	var logger *zap.Logger

	if helper.ParseBoolEnv(serviceName, "LOGGING_DISABLED") {
		logger = zap.NewNop()
	} else {
		logger = zap.Must(zap.NewProduction())
	}

	logger = logger.Named(serviceName)

	tracingLogger := logger.Named("tracing")

	if helper.ParseBoolEnv(serviceName, "TRACING_DISABLED") {
		tracingLogger.Info("tracing disabled")
	} else {
		internal.InitTracing(serviceName, tracingLogger)
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	defer signal.Stop(interrupt)

	grp, grpCtx := errgroup.WithContext(context.Background())

	var (
		pprofServer   *server.PProfServer
		metricsServer *server.MetricsServer
		grpcServer    *server.GRPCServer
		httpServer    *server.HTTPServer
		uiServer      *server.UIServer
	)

	grpcClientsService := client.NewGrpcClientsService(serviceName, logger.Named("client_grpc"))

	if helper.ParseBoolEnv(serviceName, "GRPC_ENABLED") {
		grp.Go(func() error {
			grpcServer = server.NewGRPCServer(serviceName, grpcClientsService, logger.Named("server_grpc"))

			return grpcServer.ServeGRPC()
		})
	}

	if helper.ParseBoolEnv(serviceName, "HTTP_ENABLED") {
		grp.Go(func() error {
			httpServer = server.NewHTTPServer(serviceName, grpcClientsService, logger.Named("server_http"))

			return httpServer.ServeHTTP()
		})
	}

	if helper.ParseBoolEnv(serviceName, "UI_ENABLED") {
		grp.Go(func() error {
			uiServer = server.NewUIServer(serviceName, logger.Named("server_ui"))

			return uiServer.ServeUI()
		})
	}

	if helper.ParseBoolEnv(serviceName, "METRICS_ENABLED") {
		grp.Go(func() error {
			metricsServer = server.NewMetricsServer(logger.Named("server_metrics"))

			return metricsServer.ServeMetrics()
		})
	}

	if helper.ParseBoolEnv(serviceName, "PPROF_ENABLED") {
		grp.Go(func() error {
			pprofServer = server.NewPProfServer(logger.Named("server_pprof"))

			return pprofServer.ServePProf()
		})
	}

	select {
	case <-interrupt:
		break
	case <-grpCtx.Done():
		break
	}

	shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), server.Timeout)
	defer shutdownCancel()

	if pprofServer != nil {
		pprofServer.Shutdown(shutdownCtx)
	}

	if metricsServer != nil {
		metricsServer.Shutdown(shutdownCtx)
	}

	if httpServer != nil {
		httpServer.Shutdown(shutdownCtx)
	}

	if uiServer != nil {
		uiServer.Shutdown(shutdownCtx)
	}

	if grpcServer != nil {
		grpcServer.Shutdown()
	}

	if err := grp.Wait(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			logger.Error(fmt.Sprintf("%v", err))
		}
	}
}
