fullnameOverride: tempo

ingester:
  replicas: 2 # 3
  persistence:
    enabled: true # false
    size: 10Gi
    storageClass: local-path

metricsGenerator:
  enabled: true
  replicas: 1
  # -- Grace period to allow the metrics-generator to shutdown before it is killed. Especially for the ingestor,
  # this must be increased. It must be long enough so metrics-generators can be gracefully shutdown flushing/transferring
  # all data and to successfully leave the member ring on shutdown.
  terminationGracePeriodSeconds: 300
  config:
    storage:
      # -- A list of remote write endpoints.
      # -- https://prometheus.io/docs/prometheus/latest/configuration/configuration/#remote_write
      remote_write:
        - url: "http://prometheus-kube-prometheus-prometheus.prometheus.svc:9090/api/v1/write"

distributor:
  replicas: 1
  config:
    # -- Enable to log every received span to help debug ingestion or calculate span error distributions using the logs
    log_received_spans:
      enabled: false
      include_all_attributes: false
      filter_by_status_error: false

compactor:
  replicas: 1

querier:
  replicas: 1
  config:
    frontend_worker:
      # -- grpc client configuration
      grpc_client_config: {}

queryFrontend:
  query:
    enabled: false # default false
  replicas: 1

search:
  # -- Enable Tempo search
  enabled: true # default false

multitenancyEnabled: false

traces:
  jaeger:
    grpc:
      # -- Enable Tempo to ingest Jaeger GRPC traces
      enabled: true # default false
      # -- Jaeger GRPC receiver config
      receiverConfig: {}
    thriftBinary:
      # -- Enable Tempo to ingest Jaeger Thrift Binary traces
      enabled: true # default false
      # -- Jaeger Thrift Binary receiver config
      receiverConfig: {}
    thriftCompact:
      # -- Enable Tempo to ingest Jaeger Thrift Compact traces
      enabled: true # default false
      # -- Jaeger Thrift Compact receiver config
      receiverConfig: {}
    thriftHttp:
       # -- Enable Tempo to ingest Jaeger Thrift HTTP traces
      enabled: true # default false
      # -- Jaeger Thrift HTTP receiver config
      receiverConfig: {}
  zipkin:
    # -- Enable Tempo to ingest Zipkin traces
    enabled: true # default false
    # -- Zipkin receiver config
    receiverConfig: {}
  otlp:
    http:
      # -- Enable Tempo to ingest Open Telemetry HTTP traces
      enabled: true # default false
      # -- HTTP receiver advanced config
      receiverConfig: {}
    grpc:
      # -- Enable Tempo to ingest Open Telemetry GRPC traces
      enabled: true # default false
      # -- GRPC receiver advanced config
      receiverConfig: {}
  opencensus:
    # -- Enable Tempo to ingest Open Census traces
    enabled: true # default false
    # -- Open Census receiver config
    receiverConfig: {}
  # -- Enable Tempo to ingest traces from Kafka. Reference: https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver/kafkareceiver
  kafka: {}


server:
  # -- Log format. Can be set to logfmt (default) or json.
  logFormat: logfmt # default logfmt
storage:
  trace:
    # -- The supported storage backends are gcs, s3 and azure, as specified in https://grafana.com/docs/tempo/latest/configuration/#storage
    backend: s3 # default local
    s3:
      bucket: tempo
      endpoint: minio.minio.svc:9000
      region: null
      access_key: tempo-access-key
      secret_key: tempo-secret-key
      insecure: true
      forcepathstyle: true

# memcached is for all of the Tempo pieces to coordinate with each other.
# you can use your self memcacherd by set enable: false and host + service
memcached:
  # -- Specified whether the memcached cachce should be enabled
  enabled: true  # default true
  host: memcached
  replicas: 1

memcachedExporter:
  enabled: true # default false

serviceMonitor:
  enabled: true  # default false

prometheusRule:
  enabled: true  # default false

gateway:
  enabled: true # default false
